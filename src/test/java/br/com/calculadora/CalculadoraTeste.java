package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);
        System.out.println(resultado);
        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros(){
        int resultado = calculadora.divisao(6, 2);
        System.out.println(resultado);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaADivisaoDeNumerosNegativos(){
        int resultado = calculadora.divisao(6, -2);
        System.out.println(resultado);
        Assertions.assertEquals(-3, resultado);
    }

    @Test
    public void testeADivisaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.divisao(66.3, 3.5);
        System.out.println(resultado);
        Assertions.assertEquals(18.942857143, resultado);
    }

    @Test
    public void testeAMultiplicacaoDeDoisNumerosInteiros(){
        int resultado = calculadora.multiplicacao(5, 5);
        System.out.println(resultado);
        Assertions.assertEquals(25, resultado);
    }

    @Test
    public void testeAMultiplicacaoDeDoisNumerosNegativos(){
        int resultadoNegativo = calculadora.multiplicacao(2, -4);
        System.out.println(resultadoNegativo);
        Assertions.assertEquals(-8, resultadoNegativo);
    }

    @Test
    public void testeAMultiplicacaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.multiplicacao(35.43, 3.4);
        System.out.println(resultado);
        Assertions.assertEquals(120.462, resultado);
    }

    @Test
    public void testaCaminhoTristeDaSoma(){
        Assertions.assertThrows(RuntimeException.class, () -> {calculadora.divisao(-1, 0);});
    }
}


















