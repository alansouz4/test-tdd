package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public Calculadora(){
    }

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }


    public int divisao(int primeiroNumero, int segundoNumero) {
        if(primeiroNumero < 0 && segundoNumero < 0){
            int resultadoNegativo = primeiroNumero / segundoNumero;
            return resultadoNegativo;
        }
        int retultado = primeiroNumero / segundoNumero;
        return retultado;
    }

//    public int divisaoNegativa(int primeiroNumero, int segundoNumero){
//        return -3;
//    }

    public double divisao(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero / segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(9, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public int multiplicacao(int primeiroNumero, int segundoNumero) {
        if(primeiroNumero < 0 && segundoNumero < 0){
            int resultadoNegativo = primeiroNumero * segundoNumero;
            return resultadoNegativo;
        }
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public double multiplicacao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero * segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }
}
